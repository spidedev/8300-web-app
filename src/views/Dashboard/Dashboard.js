import React from "react";
// import { useFetch } from "components/hook";
import { useState, useEffect } from "react";
import axios from "axios";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
// import Icon from "@material-ui/core/Icon";
// @material-ui/icons
// import Store from "@material-ui/icons/Store";
// import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
// import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import AvTimerIcon from "@material-ui/icons/AvTimer";
// import BugReport from "@material-ui/icons/BugReport";
// import Code from "@material-ui/icons/Code";
// import Cloud from "@material-ui/icons/Cloud";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
// import Tasks from "components/Tasks/Tasks.js";
// import CustomTabs from "components/CustomTabs/CustomTabs.js";
// import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import Button from "@material-ui/core/Button";
// import { bugs, website, server } from "variables/general.js";
import {
  // dailySalesChart,
  // emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";

const useStyles = makeStyles(styles);

export default function Dashboard() {
  const classes = useStyles();

  const urlProxy = "https://cors-anywhere.herokuapp.com/";
  // eslint-disable-next-line no-unused-vars
  const urlTotalEffect =
    urlProxy +
    "https://0498397e.ngrok.io/8300/p8300/index.php?action=getEffTotal";
  const urlLastSponsor =
    urlProxy +
    "https://0498397e.ngrok.io/8300/p8300/index.php?action=getLastSponsor";
  const urlGetNbWinner =
    urlProxy +
    "https://0498397e.ngrok.io/8300/p8300/index.php?action=getNbWinner";

  const [totalEffect, setTotalEffect] = useState([]);
  const [lastSponsor, setLastSponsor] = useState([]);
  const [getNbWinner, setGetNbWinner] = useState([]);

  useEffect(() => {
    axios.get(urlTotalEffect).then(json => setTotalEffect(json.data));
    axios.get(urlLastSponsor).then(json => setLastSponsor(json.data));
    axios.get(urlGetNbWinner).then(json => setGetNbWinner(json.data));
  }, []);

  // useEffect(() => {
  //   axios.get(urllastUsers).then(json => setLastUsers(json.data));
  // }, []);

  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="warning" stats icon>
              <CardIcon color="warning">
                <AccountCircleIcon />
              </CardIcon>
              <p className={classes.cardCategory}>Total inscrit</p>
              <h3 className={classes.cardTitle}>
                <small>
                  <code>{JSON.stringify(totalEffect.nbSub)}</code>
                </small>
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <DateRange />
                Date du jour
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="success" stats icon>
              <CardIcon color="success">
                <SupervisorAccountIcon />
              </CardIcon>
              <p className={classes.cardCategory}>Dernier inscrit</p>
              <h3 className={classes.cardTitle}>
                <code>{JSON.stringify(lastSponsor.lastName)}</code>
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <DateRange />
                Position et gains
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="danger" stats icon>
              <CardIcon color="danger">
                <AvTimerIcon />
              </CardIcon>
              <p className={classes.cardCategory}>Nombre de Gagnants</p>
              <h3 className={classes.cardTitle}>
                <code>{JSON.stringify(getNbWinner.nbWinner)}</code>
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <LocalOffer />
                SPIDER TECHNOLOGIES
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={3}>
          <Card>
            <CardHeader color="info" stats icon>
              <CardIcon color="info">
                <Accessibility />
              </CardIcon>
              <p className={classes.cardCategory}>Q5</p>
              <h3 className={classes.cardTitle}>15</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Update />
                Actualisé à l{"'"}instant
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
      <GridContainer>
        <GridItem xs={12} sm={12} md={8}>
          <Card>
            <CardHeader color="success">
              <h4 className={classes.cardTitleWhite}>
                Liste des dix derniers inscrits
              </h4>
              {/* <p className={classes.cardCategoryWhite}>
                New employees on 15th September, 2016
              </p> */}
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="success"
                tableHead={[
                  "Date",
                  "Nom & prénom(s)",
                  "Type",
                  "Option",
                  "Gain",
                  "Gain Qualification"
                ]}
                tableData={[
                  [
                    "12/12/2019",
                    "Digbeu Gregoire",
                    "Q1",
                    "A",
                    "5500f",
                    "50000f"
                  ]
                ]}
              />
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="danger">
              <ChartistGraph
                className="ct-chart"
                data={completedTasksChart.data}
                type="Line"
                options={completedTasksChart.options}
                listener={completedTasksChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>
                L{"' "}
                {"é"}tat d{"'"}évolution des inscriptions
              </h4>
              <p className={classes.cardCategory}>Hausse des inscriptions</p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> Actualisé à ce jour
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer>
      {/* <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="success">
              <h4 className={classes.cardTitleWhite}>
                Liste des dix derniers inscrits
              </h4>
              {/* <p className={classes.cardCategoryWhite}>
                New employees on 15th September, 2016
              </p> 
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="success"
                tableHead={[
                  "Date",
                  "Nom & prénom(s)",
                  "Type",
                  "Option",
                  "Gain",
                  "Gain Qualification"
                ]}
                tableData={[
                  [
                    "12/12/2019",
                    "Digbeu Gregoire",
                    "Q1",
                    "A",
                    "5500f",
                    "50000f"
                  ]
                ]}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer> */}
    </div>
  );
}

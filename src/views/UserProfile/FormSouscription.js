import React from "react";
import { useState } from "react";
// import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Checkbox from '@material-ui/core/Checkbox';
// import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
// import Box from '@material-ui/core/Box';
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
// import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import MenuItem from "@material-ui/core/MenuItem";
// import InputLabel from '@material-ui/core/InputLabel';
// import Select from '@material-ui/core/Select';
// import FormHelperText from '@material-ui/core/FormHelperText';
// import FormControl from '@material-ui/core/FormControl';
import logo from "../../assets/img/logo_8-300-01.png";
// import { BrowserRouter as Router } from "react-router-dom";
// import { withRouter } from 'react-router-dom';
import axios from "axios";
// import CircularProgress from "@material-ui/core/CircularProgress";
// import Routes from "../src/Routes";
// import CardListEnreg from './Components/cardListTotal'
// import CardListDetail from './Components/CardListDetail'
// import CardListDetail2 from './Components/CardListDetail2'
// import { Paper } from "@material-ui/core";
// import List10Pers from './Components/List10Pers'

// function Copyright() {
//   return (
//     <Typography variant="body2" color="textSecondary" align="center">
//       {'Copyright © '}
//       <Link color="inherit" href="https://material-ui.com/">
//         Your Website
//       </Link>{' '}
//       {new Date().getFullYear()}
//       {'.'}
//     </Typography>
//   );
// }

const typePiece = [
  { value: "CNI", label: "CNI" },
  { value: "Permis de conduire", label: "Permis de conduire" },
  { value: "Carte Consulaire", label: "Carte Consulaire" }
]
const optionSub = [
  { value: "A", label: "A" },
  { value: "B", label: "B" },
  { value: "C", label: "C" }
]
const typeSub = [
  { value: "1", label: "Q1" },
  { value: "2", label: "Q2" },
  { value: "3", label: "Q3" },
  { value: "5", label: "Q5" }
]

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(1),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundImage: `url(${logo})`,
    backgroundColor: "white"
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: "#83ba3b"
  },
  imageseul: {
    width: 40,
    float: "center",
    position: "relative",
    overflow: "auto"
  },
  paper2: {
    padding: theme.spacing(1),
    textAlign: "left",
    color: theme.palette.text.secondary
  },
  paper3: {
    padding: theme.spacing(1),
    textAlign: "left",
    color: theme.palette.text.secondary,
    height: 150
  },

  root: {
    flexDirection: "column",
    marginTop: 10
  },
  TextField: {
    borderColor: "green"
  }
}));

export default function Sign() {
  const classes = useStyles();
  // const [nomPiece, setNomPiece] = React.useState('Choisissez');
  const [customer, setCustomer] = useState({
    sponsorPhone: "",
    lastName: "",
    firstName: "",
    phone: "",
    identityCardType: "",
    identityCardNumber: "",
    serialKey: "",
    optionSub: "",
    typeSub: "",
    birthDate: "",
    opID: 0
  });

  const [showLoading, setShowLoading] = useState(false);

  const urlProxy = "https://cors-anywhere.herokuapp.com/";
  // const apiUrl = urlProxy + 'http://localhost/8300_API/';

  const apiUrl = urlProxy + "http://fd280249.ngrok.io/8300/";

  const saveCustomer = (e) => {
    // alert(JSON.stringify(customer))
    setShowLoading(true);
    //  e.preventDefault();
    const bodyFormData = new FormData();
    bodyFormData.append("params", JSON.stringify(customer));
    bodyFormData.append("action", "registerCustomer");
    axios({
      method: "POST",
      url: apiUrl,
      data: bodyFormData,
      // headers: {'Content-Type': 'multipart/form-data'}
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    })
      .then((response) => {
        console.log(response.data);
        alert(JSON.stringify(response.data))
      })
      .catch((err) => {
        console.log(err);
        // alert(JSON.stringify(err))
      })
  };

  const onChange = (e) => {
    e.persist();
    setCustomer({ ...customer, [e.target.name]: e.target.value });
  }

  return (
    <Container>
      <CssBaseline />
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          <form className={classes.form} noValidate onSubmit={saveCustomer}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  size="small"
                  variant="outlined"
                  // required
                  fullWidth
                  id="serialKey"
                  label="Numéro de la carte"
                  name="serialKey"
                  value={customer.serialKey}
                  onChange={onChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  size="small"
                  autoComplete="nom"
                  name="firstName"
                  variant="outlined"
                  // required
                  fullWidth
                  id="firstName"
                  label="Nom"
                  value={customer.firstName}
                  onChange={onChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  size="small"
                  variant="outlined"
                  // required
                  fullWidth
                  id="lastName"
                  label="Prénom(s)"
                  name="lastName"
                  value={customer.lastName}
                  onChange={onChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  name="birthDate"
                  variant="outlined"
                  // required
                  fullWidth
                  id="birthDate"
                  label="Date de naissance"
                  value={customer.birthDate}
                  onChange={onChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  // required
                  fullWidth
                  id="phone"
                  label="Téléphone"
                  name="phone"
                  value={customer.phone}
                  onChange={onChange}
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  id="identityCardType"
                  select
                  fullWidth
                  label="Type de Pièce"
                  name="identityCardType"
                  value={customer.identityCardType}
                  onChange={onChange}
                  helperText="Rentrer une pièce d'identitée valide"
                  variant="outlined"
                >
                  {typePiece.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  // required
                  fullWidth
                  id="identityCardNumber"
                  label="Numéro de Pièce"
                  name="identityCardNumber"
                  value={customer.identityCardNumber}
                  onChange={onChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  size="small"
                  variant="outlined"
                  // required
                  fullWidth
                  id="sponsorPhone"
                  label="Téléphone du parrain"
                  name="sponsorPhone"
                  value={customer.sponsorPhone}
                  onChange={onChange}
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  id="optionSub"
                  select
                  label="Option"
                  name="optionSub"
                  value={customer.optionSub}
                  onChange={onChange}
                  helperText="Choisissez une option de qualification"
                  variant="outlined"
                >
                  {optionSub.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>

              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  id="typeSub"
                  select
                  label="Type"
                  name="typeSub"
                  value={customer.typeSub}
                  onChange={onChange}
                  helperText="Choix du type de qualification"
                  variant="outlined"
                >
                  {typeSub.map(option => (
                    <MenuItem key={option.value} value={option.value} onChange={onChange}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>
            </Grid>

            <Button
              //  type="submit"
              fullWidth
              variant="contained"
              color="green"
              className={classes.submit}
              onClick={saveCustomer}
            >
              Enregistrer
          </Button>
          </form>
        </div>
      </Container>
      {/*       <Box mt={5}>
        <Copyright />
      </Box>
           */}
    </Container>
  );
}
